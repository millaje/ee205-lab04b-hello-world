# Build several interesting Hello World programs

TARGETS=hello_from_gcc hello_from_g++ hello_from_clang hello_cpp both_worlds

all: $(TARGETS)

hello_from_gcc: hello.c
	gcc -g -Wall -o hello_from_gcc       hello.c

hello_from_g++: hello.c
	g++ -g -Wall -o hello_from_g++       hello.c

hello_from_clang: hello.c
	clang -g -Wall -o hello_from_clang   hello.c

hello_cpp: hello.cpp
	g++ -g -Wall -o hello_cpp            hello.cpp

both_worlds: both_worlds.cpp
	g++ -g -Wall -o both_worlds          both_worlds.cpp

clean:
	rm -f $(TARGETS) *.o

